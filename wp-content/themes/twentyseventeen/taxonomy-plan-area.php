<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>


<div class="wrap">

    <?php if (have_posts()) : ?>
        <header class="page-header">
            <?php
            the_archive_title('<h1 class="page-title">', '</h1>');
            the_archive_description('<div class="taxonomy-description">', '</div>');
            ?>
        </header><!-- .page-header -->
    <?php endif; ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php

            if (function_exists("p4p_get_plan_grid")) {
                echo p4p_get_plan_grid(array(
                    'tax_query'         => array(
                        array(
                            'taxonomy'    => 'plan-area',
                            'field'       => 'slug',
                            'terms'       => get_queried_object()->slug
                        )
                    )
                ));
            }

            ?>

        </main><!-- #main -->
    </div><!-- #primary -->
</div><!-- .wrap -->


<?php
get_footer();
