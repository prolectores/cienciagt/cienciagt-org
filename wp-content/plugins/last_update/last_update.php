<?php
/*
Plugin Name: Balam Last Update
Plugin URI: http://gitlab.com/balamaqab
Description: Shortcode last_update para mostrar la fecha de la última modificación de una publicación.
Version: 0.1.20200510
Author: balamaqab
Author URI: http://gitlab.com/balamaqab
*/ 


function balam_last_update( $atts ){
    setlocale(LC_ALL, 'es_ES.UTF-8');
    $current_post = get_post();
    $post_modified = strtotime($current_post->post_modified);
    $update = strftime('%B %e, %Y', $post_modified);
    
    $label = "Última actualización";
    
    if(isset($atts["label"])){
        $label = $atts["label"];
    }
    $content = '<div class="balam_last_update"><strong>'.$label .':</strong> '.$update.'</div>';
    return $content;
}
add_shortcode( 'last_update', 'balam_last_update' );

?>